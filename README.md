# Coat Recommendation

This is a code sample that I previously completed.  The challenge was to gather weather data from a publicly accessible API, transform the data, and push the result onto a service bus entity.

I decided to gather weather from multiple sources, get the most pessimistic forecast, and recommend the type of coat to wear (none, light, heavy, raincoat).  The code fetches the weather asynchronously and uses the reactive framework to process the result to push onto the service bus.

This is a good work sample for a few reasons:  the code has no StyleCop violations, the code is fully tested, and the code is an example of adapting remote APIs into reactive observables that get merged to produce a single statistic.  