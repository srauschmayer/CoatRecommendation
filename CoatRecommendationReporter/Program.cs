﻿//-----------------------------------------------------------------------
// <copyright file="Program.cs" company="Stephen Rauschmayer">
//  Copyright (c) Stephen Rauschmayer. MIT licience
// </copyright>
// <summary>
//  Report a coat recommendation to a Azure Service Bus.
// </summary>
//-----------------------------------------------------------------------

namespace CoatRecommendationReporter
{
    using System;
    using System.Collections.Generic;
    using System.Configuration;
    using System.Linq;
    using System.Reactive.Linq;
    using System.Reactive.Subjects;
    using System.Reactive.Threading.Tasks;
    using CoatRecommendation;
    using DailyWeatherProviders;
    using Microsoft.ServiceBus.Messaging;

    /// <summary>
    /// Main program that publishes a coat recommendation based on the weather
    /// </summary>
    public class Program
    {
        /// <summary>
        /// Application entry point.
        /// </summary>
        /// <param name="args">Parameter unused</param>
        public static void Main(string[] args)
        {
            try
            {
                var weatherUndergroundApiKey = ConfigurationManager.AppSettings["WeatherUnderground.ApiKey"];
                var forecastIoApiKey = ConfigurationManager.AppSettings["ForecastIo.ApiKey"];

                Console.WriteLine("Starting coat recommendation by gathering forcast data");
                var dailyForecast = GetPessemisticForecast(
                    new WeatherUndergroundForecastProvider(weatherUndergroundApiKey),
                    new ForecastIoForecastProvider(forecastIoApiKey));

                var coatRecomendation = dailyForecast.Select(CoatRecommendationStatistic.Recommendation);

                var serviceBus = GetServiceBus();
                coatRecomendation.Select(recomendation => new BrokeredMessage(recomendation.ToString()))
                                 .Subscribe(serviceBus.Send);
                coatRecomendation.Subscribe(
                    recommendation => Console.WriteLine("Publishing recomendation to queue: {0}", recommendation),
                    (Exception e) => Console.WriteLine("Something has gone wrong: {0}", e),
                    () => { });

                dailyForecast.Connect();
                Console.WriteLine("Press any key to end...");
                Console.ReadKey();
            }
            catch (Exception ex)
            {
                Console.WriteLine("General application error: {0}", ex);
            }
        }

        /// <summary>
        /// Get an aggregated forecast from the specified providers
        /// </summary>
        /// <param name="forecastProviders">A list of daily forecast provider instances to use</param>
        /// <returns>An observable that will have the aggregate forecast</returns>
        private static IConnectableObservable<DailyForecast> GetPessemisticForecast(params IDailyForecastProvider[] forecastProviders)
        {
            var dailyForecasts = forecastProviders.Select(provider => provider.GetForecast(Location.WESTCHESTER)
                                                                      .ToObservable()
                                                                      .Timeout(new TimeSpan(0, 0, 30), Observable.Return(DailyForecast.Empty))
                                                                      .Catch(Observable.Return(DailyForecast.Empty)));
            var aggregateDailyForecast = Observable.CombineLatest(dailyForecasts)
                                                   .Select(forecasts => forecasts.Aggregate(DailyForecast.Merge))
                                                   .Publish();
            aggregateDailyForecast.Subscribe(forecast => Console.WriteLine("Pessimistic Forcast: {0}", forecast));
            return aggregateDailyForecast;
        }

        /// <summary>
        /// Create a connection to an Azure service bus
        /// </summary>
        /// <returns>A queue client to the configured bus</returns>
        private static QueueClient GetServiceBus()
        {
            var queueConnectionString = ConfigurationManager.AppSettings["Microsoft.ServiceBus.ConnectionString"];
            var queueName = ConfigurationManager.AppSettings["Microsoft.ServiceBus.QueueName"];
            var queue = QueueClient.CreateFromConnectionString(queueConnectionString, queueName);
            return queue;
        }
    }
}
