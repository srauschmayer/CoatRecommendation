﻿namespace CoatRecommendtionTests
{
    using System;
    using Microsoft.VisualStudio.TestTools.UnitTesting;
    using CoatRecommendation;
    using DailyWeatherProviders;

    [TestClass]
    public class CoatRecommendationStatisticTests
    {
        public void RecommendationMatches(
            int temperature, 
            bool expectRain, 
            RecommendedCoatType expectedRecommendation)
        {
            var forecast = new DailyForecast
            {
                LowTemperature = temperature,
                PrecipitationExpected = expectRain
            };
            var actualRecommendation = CoatRecommendationStatistic.Recommendation(forecast);
            Assert.AreEqual(expectedRecommendation, actualRecommendation);
        }

        [TestMethod]
        public void WhenTheforecastIsZeroRecommendHeavyCoat()
        {
            RecommendationMatches(0, false, RecommendedCoatType.Heavy);
        }

        [TestMethod]
        public void WhenTheforecastIsThirtyNineRecommendHeavyCoat()
        {
            RecommendationMatches(39, false, RecommendedCoatType.Heavy);
        }

        [TestMethod]
        public void WhenTheforecastIsFourtyRecommendLightCoat()
        {
            RecommendationMatches(40, false, RecommendedCoatType.Light);
        }

        [TestMethod]
        public void WhenTheforecastIsSixtyFourRecommendLightCoat()
        {
            RecommendationMatches(64, false, RecommendedCoatType.Light);
        }

        [TestMethod]
        public void WhenTheforecastIsSixtyFiveRecommendNoCoat()
        {
            RecommendationMatches(65, false, RecommendedCoatType.None);
        }

        [TestMethod]
        public void WhenTheforecastIsPercipitationRecommendRaincoat()
        {
            RecommendationMatches(66, true, RecommendedCoatType.Raincoat);
        }

        [TestMethod]
        public void WhenTheforecastIsPercipitationAndZeroRecommendRaincoat()
        {
            RecommendationMatches(0, true, RecommendedCoatType.Raincoat);
        }
    }
}
