﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace CoatRecommendtionTests
{
    using DailyWeatherProviders;

    [TestClass]
    public class forecastTests
    {
        static readonly DailyForecast HOT_AND_WET = new DailyForecast
        {
            LowTemperature = 90,
            PrecipitationExpected = true
        };

        static readonly DailyForecast COLD_AND_DRY = new DailyForecast
        {
            LowTemperature = 0,
            PrecipitationExpected = false
        };

        [TestMethod]
        public void WhenMergingAnEmptyforecastAgainstAforecastReturnTheforecast()
        {
            var resultforecast = DailyForecast.Merge(DailyForecast.Empty, HOT_AND_WET);
            Assert.AreEqual(HOT_AND_WET, resultforecast);
        }

        [TestMethod]
        public void WhenMergingAforecastAgainstAnEmptyforecastReturnTheforecast()
        {
            var resultforecast = DailyForecast.Merge(HOT_AND_WET, DailyForecast.Empty);
            Assert.AreEqual(HOT_AND_WET, resultforecast);
        }

        [TestMethod]
        public void WhenMergingTwoforecastReturnTheMinimumTempature()
        {
            var resultforecast = DailyForecast.Merge(HOT_AND_WET, COLD_AND_DRY);
            var expectedTemperature = COLD_AND_DRY.LowTemperature;
            var actualTemperature = resultforecast.LowTemperature;
            Assert.AreEqual(expectedTemperature, actualTemperature);
        }

        [TestMethod]
        public void WhenMergingTwoforecastReturnAnyChanceOfRain()
        {
            var resultforecast = DailyForecast.Merge(HOT_AND_WET, COLD_AND_DRY);
            var expectedPercipitation = HOT_AND_WET.PrecipitationExpected;
            var actualPercipitation = resultforecast.PrecipitationExpected;
            Assert.AreEqual(expectedPercipitation, actualPercipitation);
        }
    }
}
