﻿//-----------------------------------------------------------------------
// <copyright file="CoatRecommendationStatistic.cs" company="Stephen Rauschmayer">
//  Copyright (c) Stephen Rauschmayer. MIT licience
// </copyright>
// <summary>
//  Define a recommendation for type of coat to wear.
// </summary>
//-----------------------------------------------------------------------

namespace CoatRecommendation
{
    using System;
    using DailyWeatherProviders;

    /// <summary>
    /// Recommends the type of coat to wear for a given forecast.
    /// </summary>
    public static class CoatRecommendationStatistic
    {
        /// <summary>
        /// Recommend the type of coat to wear for the given forecast
        /// </summary>
        /// <remarks>
        /// A coat is selected by first looking at the precipitation.  Since
        /// I have one raincoat and my other coats fair poorly in the rain,
        /// that wins if it is raining.  Otherwise, I can choose from my heavy
        /// and light coats depending on the low temperature for the day.
        /// </remarks>
        /// <param name="conditions">The forecast for the day</param>
        /// <returns>The recommended coat to wear</returns>
        public static RecommendedCoatType Recommendation(DailyForecast conditions)
        {
            var coatBasedOnPrecipitation = BasedOnPrecipitation(conditions.PrecipitationExpected);
            var coatBasedOnTempature = BasedOnTemperature(conditions.LowTemperature);
            var recommendation = coatBasedOnPrecipitation != RecommendedCoatType.Unknown ? coatBasedOnPrecipitation : 
                                                                                           coatBasedOnTempature;

            Console.WriteLine("Recommending coat {0} for forecast {1}.", recommendation, conditions);
            return recommendation;
        }

        /// <summary>
        /// Recommend a coat type based on the temperature
        /// </summary>
        /// <param name="temperature">A temperature in F</param>
        /// <returns>A coat recommendation based on the daily low temperature</returns>
        private static RecommendedCoatType BasedOnTemperature(double temperature)
        {
            if (temperature < 40)
            {
                return RecommendedCoatType.Heavy;
            }
            else if (temperature < 65)
            {
                return RecommendedCoatType.Light;
            }
            else
            {
                return RecommendedCoatType.None;
            }
        }

        /// <summary>
        /// Recommend a coat type based on the presence of precipitation
        /// </summary>
        /// <param name="participationExpected">Precipitation expected</param>
        /// <returns>A coat recommendation based on precipitation</returns>
        private static RecommendedCoatType BasedOnPrecipitation(bool participationExpected)
        {
            if (participationExpected)
            {
                return RecommendedCoatType.Raincoat;
            }
            else
            {
                return RecommendedCoatType.Unknown;
            }
        }
    }
}
