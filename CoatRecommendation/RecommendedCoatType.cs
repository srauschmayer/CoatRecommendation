﻿//-----------------------------------------------------------------------
// <copyright file="RecommendedCoatType.cs" company="Stephen Rauschmayer">
//  Copyright (c) Stephen Rauschmayer. MIT licience
// </copyright>
// <summary>
//  Define coats that can be recommended based on what I generally wear.
// </summary>
//-----------------------------------------------------------------------

namespace CoatRecommendation
{
    /// <summary>
    /// Types of coasts that can be recommended.
    /// </summary>
    public enum RecommendedCoatType
    {
        /// <summary>
        /// Don't know what to recommend
        /// </summary>
        Unknown,

        /// <summary>
        /// No coat needed
        /// </summary>
        None,

        /// <summary>
        /// A light coat is needed
        /// </summary>
        Light,
        
        /// <summary>
        /// A heavy coat is needed
        /// </summary>
        Heavy,
        
        /// <summary>
        /// A water resistant coat is needed
        /// </summary>
        Raincoat
    }
}
