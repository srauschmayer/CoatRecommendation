﻿//-----------------------------------------------------------------------
// <copyright file="Location.cs" company="Stephen Rauschmayer">
//  Copyright (c) Stephen Rauschmayer. MIT licience
// </copyright>
// <summary>
//  Define a class to represent a geographical location.
// </summary>
//-----------------------------------------------------------------------

namespace DailyWeatherProviders
{
    /// <summary>
    /// A location that has weather.
    /// </summary>
    /// <remarks>
    /// The location class specifies a location by a superset of identifiers
    /// that are used in the various weather APIs.
    /// </remarks>
    public partial class Location
    {
        /// <summary>
        /// Gets or sets the name of the city containing the location
        /// </summary>
        public string City { get; set; }

        /// <summary>
        /// Gets or sets the name of the state containing the location
        /// </summary>
        public string State { get; set; }
        
        /// <summary>
        /// Gets or sets the name of the country containing the location
        /// </summary>
        public string Country { get; set; }
        
        /// <summary>
        /// Gets or sets the absolute latitude of the location
        /// </summary>
        public float Latitude { get; set; }
        
        /// <summary>
        /// Gets or sets the absolute longitude of the location
        /// </summary>
        public float Longitude { get; set; }
    }

    /// <summary>
    /// A list of known locations
    /// </summary>
    /// <remarks>
    /// Separated from the other class for readability.
    /// </remarks>
    public partial class Location
    {
        /// <summary>
        ///  West Chester, PA
        /// </summary>
        public static readonly Location WESTCHESTER = new Location
        {
            City = "West Chester",
            State = "PA",
            Country = "US",
            Latitude = 39.9586f,
            Longitude = -75.605434f
        };
    }
}
