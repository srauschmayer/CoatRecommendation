﻿//-----------------------------------------------------------------------
// <copyright file="DailyForecast.cs" company="Stephen Rauschmayer">
//  Copyright (c) Stephen Rauschmayer. MIT licience
// </copyright>
// <summary>
//  Define a type to represent a forecast in our application.
// </summary>
//-----------------------------------------------------------------------
 
namespace DailyWeatherProviders
{
    using System;

    /// <summary>
    ///  The DailyForecast class holds a simplified view of a forecast for a 
    ///  particular day.
    /// </summary>
    public class DailyForecast
    {
        /// <summary>
        ///  An empty forecast indicating there is no information known about the day.
        /// </summary>
        public static readonly DailyForecast Empty = new DailyForecast()
        {
            EmptyForecast = true
        };

        /// <summary>
        ///  Gets or sets a value indicating whether it is at least as likely 
        ///  to rain today than not rain.
        /// </summary>
        public bool PrecipitationExpected { get; set; }

        /// <summary>
        /// Gets or sets the daily low temperature in F.  
        /// </summary>
        /// <remarks>
        /// The daily low temperature is designed to be the 'feels like'
        /// temperature if available.  Otherwise, absolute temperature is used
        /// as a proxy.
        /// </remarks>
        public double LowTemperature { get; set; }

        /// <summary>
        ///  Gets or sets a value indicating whether a class represents an Empty Forecast
        /// </summary>
        /// <remarks>
        /// The merge needs to know that a DailyForecast represents the empty  
        /// forecast to avoid merging. This is a degeneration of the null object 
        /// pattern, but simplifies the implementation of Merge.
        /// </remarks>
        private bool EmptyForecast { get; set; }

        /// <summary>
        ///  Pessimistic merge of forecasts.
        /// </summary>
        /// <remarks>Assumes that cold and wet weather is the worst.</remarks>
        /// <param name="first">
        /// The first forecast to be merged.  
        /// It is potentially empty (nulls are treated as empty).
        /// </param>
        /// <param name="second">
        /// The second forecast to be merged.  
        /// It is potentially empty (nulls are treated as empty).
        /// </param>
        /// <returns>
        /// A forecast that returns the worst weather in either forecast for
        /// each included property.  If either forecast is empty, the other forecast 
        /// should be return, or an empty forecast if both inputs are empty.
        /// </returns>
        public static DailyForecast Merge(DailyForecast first, DailyForecast second)
        {
            // Convert null parameters into Empty Forecasts
            first = first ?? DailyForecast.Empty;
            second = second ?? DailyForecast.Empty;

            if (first.EmptyForecast)
            {
                return second;
            }
            else if (second.EmptyForecast)
            {
                return first;
            }
            else
            {
                return new DailyForecast
                {
                    PrecipitationExpected = first.PrecipitationExpected || second.PrecipitationExpected,
                    LowTemperature = Math.Min(first.LowTemperature, second.LowTemperature)
                };
            }
        }

        /// <summary>
        /// Convert DailyForecast as a string
        /// </summary>
        /// <returns>A string representation of DailyForecast</returns>
        public override string ToString()
        {
            return string.Format("Format({0}, {1})", this.LowTemperature, this.PrecipitationExpected ? "Wet" : "Dry");
        }
    }
}
