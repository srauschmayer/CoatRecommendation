﻿//-----------------------------------------------------------------------
// <copyright file="IDailyForecastProvider.cs" company="Stephen Rauschmayer">
//  Copyright (c) Stephen Rauschmayer. MIT licience
// </copyright>
// <summary>
//  Define an interface to get forecast data
// </summary>
//-----------------------------------------------------------------------

namespace DailyWeatherProviders
{
    using System.Threading.Tasks;

    /// <summary>
    ///  The IDailyForecast class provides an interface to get daily forecast data
    /// </summary>
    public interface IDailyForecastProvider
    {
        /// <summary>
        /// Return a forecast for today's weather at the given location.
        /// </summary>
        /// <param name="location">The location to provide weather for</param>
        /// <returns>A task that retrieves the forecast for today</returns>
        Task<DailyForecast> GetForecast(Location location);
    }
}
