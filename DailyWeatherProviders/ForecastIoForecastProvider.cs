﻿//-----------------------------------------------------------------------
// <copyright file="ForecastIoForecastProvider.cs" company="Stephen Rauschmayer">
//  Copyright (c) Stephen Rauschmayer. MIT licience
// </copyright>
// <summary>
//  Define an adaptor for the ForecastIO weather API
//  https://developer.forecast.io/docs/v2
// </summary>
//-----------------------------------------------------------------------

namespace DailyWeatherProviders
{
    using System;
    using System.Linq;
    using System.Threading.Tasks;
    using ForecastIO;

    /// <summary>
    /// Adaptor for the ForecastIO library as a IDailyForecastProvider.
    /// </summary>
    public class ForecastIoForecastProvider : IDailyForecastProvider
    {
        /// <summary>
        /// The API key for the Forecast.IO account being used.
        /// </summary>
        private string apiKey;

        /// <summary>
        /// Initializes a new instance of the <see cref="ForecastIoForecastProvider" /> class with the provided key.
        /// </summary>
        /// <param name="apiKey">The API key for the account to be used</param>
        public ForecastIoForecastProvider(string apiKey)
        {
            this.apiKey = apiKey;
        }

        /// <summary>
        /// Get the daily forecast for the provided location using the ForecastIO API.
        /// </summary>
        /// <param name="location">The location of the forecast</param>
        /// <returns>A task returning today's daily forecast</returns>
        public Task<DailyForecast> GetForecast(Location location)
        {
            return Task.Factory.StartNew(() =>
            {
                // Return forecast in F to remain compatible with our internal types without neededing conversion.
                var request = new ForecastIORequest(this.apiKey, location.Latitude, location.Longitude, ForecastIO.Unit.us);
                var result = request.Get();
                var currentforecast = result.daily.data.First();
                var forcast = new DailyForecast
                {
                    PrecipitationExpected = currentforecast.precipProbability > .5,
                    LowTemperature = currentforecast.apparentTemperatureMin
                };

                Console.WriteLine("Forecast.IO forecast: {0}", forcast);

                return forcast;
            });
        }
    }
}
