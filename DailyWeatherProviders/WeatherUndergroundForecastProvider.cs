﻿//-----------------------------------------------------------------------
// <copyright file="WeatherUndergroundForecastProvider.cs" company="Stephen Rauschmayer">
//  Copyright (c) Stephen Rauschmayer. MIT licience
// </copyright>
// <summary>
//  Define an adaptor for the Weather Underground weather API
//  http://www.wunderground.com/weather/api/
// </summary>
//-----------------------------------------------------------------------

namespace DailyWeatherProviders
{
    using System;
    using System.Linq;
    using System.Threading.Tasks;
    using CGurus.Weather.WundergroundAPI;
    using CGurus.Weather.WundergroundAPI.Models;

    /// <summary>
    /// Adaptor for the WeatherUnderground library as a IDailyForecastProvider.
    /// </summary>
    public class WeatherUndergroundForecastProvider : IDailyForecastProvider
    {
        /// <summary>
        /// The API key for the WeatherUnderground account being used.
        /// </summary>
        private string apiKey;

        /// <summary>
        /// Initializes a new instance of the <see cref="WeatherUndergroundForecastProvider" /> class with the provided key.
        /// </summary>
        /// <param name="apiKey">The API key for the account to be used</param>
        public WeatherUndergroundForecastProvider(string apiKey)
        {
            this.apiKey = apiKey;
        }

        /// <summary>
        /// Get the daily forecast for the provided location using the WeatherUnderground API.
        /// </summary>
        /// <param name="location">The location of the forecast</param>
        /// <returns>A task returning today's daily forecast</returns>
        public Task<DailyForecast> GetForecast(Location location)
        {
            return Task.Factory.StartNew(() =>
            {
                WApi undergroundAPI = new WApi(this.apiKey);
                var currentforecastResult = undergroundAPI.GetForecastUS(location.State, location.City);

                // Get today's forcast from the list of forcasts.
                var currentforecast = currentforecastResult.Forecast.SimpleForecast.ForecastDay.First();
                var expectPrecipitation = currentforecast.Pop > 50;
                var tempature = currentforecast.Low.Fahrenheit;
                var forcast = new DailyForecast
                {
                    PrecipitationExpected = expectPrecipitation,
                    LowTemperature = tempature.Value
                };

                Console.WriteLine("WeatherUnderground forecast: {0}", forcast);

                return forcast;
            });
        }
    }
}
